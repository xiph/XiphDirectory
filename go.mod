module gitlab.xiph.org/xiph/XiphDirectory

go 1.14

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/DATA-DOG/go-sqlmock v1.4.1
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/gin v1.6.3
	github.com/jinzhu/gorm v1.9.12
	github.com/lib/pq v1.5.1
	github.com/robfig/cron v1.2.0
	github.com/rs/zerolog v1.18.0
	github.com/satori/go.uuid v1.2.0
	github.com/stretchr/testify v1.5.1
)
