package config

import (
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"
)

var defaultConfig Config

func init() {
	LoadDefaults()
	defaultConfig = C
	C = Config{}
}

func cleanupTestLoadConfigNonExisting() {
	os.Remove(filepath.Join("..", "..", "test", "data", "config_non_existing.toml"))
}

func cleanupConfig() {
	C = Config{}
}

func TestLoadConfigDecoderError(t *testing.T) {
	assert.Empty(t, C, "Config should be empty on beginning")

	err := LoadConfig(filepath.Join("..", "..", "test", "data", "config_malformed.toml"))

	assert.EqualError(t, err, "Near line 8 (last key parsed 'Web'): expected a top-level item to end with a newline, comment, or EOF, but got ']' instead", "Reading a malformed config should cause an error")
	t.Cleanup(cleanupConfig)
}

func TestLoadConfigNonExisting(t *testing.T) {
	assert.Empty(t, C, "Config should be empty on beginning")

	err := LoadConfig(filepath.Join("..", "..", "test", "data", "config_non_existing.toml"))

	assert.NoError(t, err, "Loading a non existing config should not return an error")
	assert.Equal(t, defaultConfig, C, "The config should be equal to the default config")

	t.Cleanup(cleanupTestLoadConfigNonExisting)
	t.Cleanup(cleanupConfig)
}

func TestLoadConfigNonExistingWriteError(t *testing.T) {
	assert.Empty(t, C, "Config should be empty on beginning")

	err := LoadConfig(filepath.Join("..", "..", "test", "data", "nonexisting", "config_non_existing.toml"))

	assert.EqualError(t, err, "open ../../test/data/nonexisting/config_non_existing.toml: no such file or directory", "Writing to a non existing folder should retrurn an error")
	t.Cleanup(cleanupConfig)
}
