// Package config provides the XiphDirectory config file implementation
package config

import (
	"bytes"
	"errors"
	"io/ioutil"
	"os"
	"path/filepath"

	"github.com/BurntSushi/toml"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

// Config main type
type Config struct {
	YP struct {
		VerifyListenURL        bool
		TouchFreq              int
		MissedTouchesThreshold int
		EnableBanlist          bool
		Banlist                string
	}

	Web struct {
		BindUnix          bool
		Bind              string
		WebDir            string
		TrustProxyHeaders bool
		XMLCacheTime      int
		PageSize          int
	}

	Database struct {
		Connection string
	}

	Log struct {
		LogToFile bool
		Filename  string
	}
}

// C holds the loaded configuration
var C Config

var slog zerolog.Logger

// LoadDefaults puts default values into C
func LoadDefaults() {
	C.YP.VerifyListenURL = true
	C.YP.TouchFreq = 200
	C.YP.MissedTouchesThreshold = 2
	C.YP.EnableBanlist = true
	C.YP.Banlist = "./configs/banlist.toml"

	C.Web.BindUnix = false
	C.Web.Bind = ":8000"
	C.Web.WebDir = "./web"
	C.Web.TrustProxyHeaders = true
	C.Web.XMLCacheTime = 5
	C.Web.PageSize = 30

	C.Database.Connection = "host=localhost user=directory dbname=directory password=directory sslmode=disable"

	C.Log.LogToFile = false
	C.Log.Filename = "../directory.log"
}

// LoadConfig loads the config found at the given path
func LoadConfig(path string) error {
	slog = log.With().Str("component", "config").Logger()
	LoadDefaults()
	_, err := toml.DecodeFile(path, &C)
	if errors.Is(err, os.ErrNotExist) {
		slog.Warn().Str("path", path).Msg("Could not find file, using defaults!")
		err = WriteConfig(path)
		if err != nil {
			return err
		}
	} else if err != nil {
		slog.Error().Str("path", path).Err(err).Send()
		return err
	}
	path, _ = filepath.Abs(path)
	slog.Info().Str("path", path).Msg("Config loaded")
	return err
}

// WriteConfig writes the configuration to the given path
func WriteConfig(path string) error {
	buf := new(bytes.Buffer)
	err := toml.NewEncoder(buf).Encode(C)
	if err != nil {
		slog.Err(err).Send()
		return err
	}
	err = ioutil.WriteFile(path, buf.Bytes(), 0600)
	if err != nil {
		slog.Err(err).Send()
		return err
	}
	slog.Info().Str("path", path).Msg("Wrote config")
	return err
}
