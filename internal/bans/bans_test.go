package bans

import (
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.xiph.org/xiph/XiphDirectory/internal/config"
)

var defaultBans bl

func init() {
	defaultBans.Bans = append(defaultBans.Bans, ban{
		Reason:       "Private IP address range",
		IPs:          []string{"127.0.0.0/8", "10.0.0.0/8", "172.16.0.0/12", "192.168.0.0/16", "169.254.0.0/16"},
		HostPatterns: []string{".*\\.local"},
	})
	defaultBans, _ = processBanlist(defaultBans)
}

func cleanupTestLoadBanlistNonExisting() {
	os.Remove(filepath.Join("..", "..", "test", "data", "banlist_non_existing.toml"))
}

func cleanupBanlist() {
	banlist = bl{}
}

func TestProcessBanlistInvalidRegex(t *testing.T) {
	var testbanlist bl
	testbanlist.Bans = append(testbanlist.Bans, ban{
		Reason:       "Private IP address range",
		IPs:          []string{"127.0.0.0/8", "10.0.0.0/8", "172.16.0.0/12", "192.168.0.0/16", "169.254.0.0/16"},
		HostPatterns: []string{"(()"},
	})
	_, errs := processBanlist(testbanlist)
	if assert.NotEmpty(t, errs, "There should be an error") {
		assert.EqualError(t, errs[0], "error parsing regexp: missing closing ): `(()`", "The error should be a regex error")
	}
}

func TestProcessBanlistInvalidCIDR(t *testing.T) {
	var testbanlist bl
	testbanlist.Bans = append(testbanlist.Bans, ban{
		Reason:       "Private IP address range",
		IPs:          []string{"127.0.0.277/8"},
		HostPatterns: []string{".*\\.local"},
	})
	_, errs := processBanlist(testbanlist)
	if assert.NotEmpty(t, errs, "There should be an error") {
		assert.EqualError(t, errs[0], "invalid CIDR address: 127.0.0.277/8", "The error should be a CIDR error")
	}
}

func TestLoadBanlistDisabled(t *testing.T) {
	config.C.YP.EnableBanlist = false
	err := LoadBanlist(filepath.Join("..", "..", "test", "data", "banlist_non_existing.toml"))
	assert.NoError(t, err, "There should be no error")
	assert.Empty(t, banlist, "Banlist should be empty if disabled")
}

func TestLoadBanlistDecoderError(t *testing.T) {
	config.C.YP.EnableBanlist = true
	assert.Empty(t, banlist, "Banlist should be empty on beginning")

	err := LoadBanlist(filepath.Join("..", "..", "test", "data", "banlist_malformed.toml"))

	assert.EqualError(t, err, "Near line 1 (last key parsed ''): expected end of table array name delimiter ']', but got '\\n' instead", "Reading a malformed banlist should cause an error")
	t.Cleanup(cleanupBanlist)
}
func TestLoadBanlistNonExisting(t *testing.T) {
	config.C.YP.EnableBanlist = true
	assert.Empty(t, banlist, "Banlist should be empty on beginning")

	err := LoadBanlist(filepath.Join("..", "..", "test", "data", "banlist_non_existing.toml"))

	assert.NoError(t, err, "Loading a non existing banlist should not return an error")
	assert.Equal(t, defaultBans, banlist, "The banlist should be equal to the default banlist")

	t.Cleanup(cleanupTestLoadBanlistNonExisting)
	t.Cleanup(cleanupBanlist)
}

func TestLoadBanlistNonExistingWriteError(t *testing.T) {
	config.C.YP.EnableBanlist = true
	assert.Empty(t, banlist, "Banlist should be empty on beginning")

	err := LoadBanlist(filepath.Join("..", "..", "test", "data", "nonexisting", "banlist_non_existing.toml"))

	assert.EqualError(t, err, "open ../../test/data/nonexisting/banlist_non_existing.toml: no such file or directory", "Writing to a non existing folder should retrurn an error")
	t.Cleanup(cleanupBanlist)
}

func TestIsBannedBanlistDisabled(t *testing.T) {
	config.C.YP.EnableBanlist = false
	banlist = defaultBans
	banned, reason := IsBanned("test.local", "10.0.0.1")
	assert.Equal(t, false, banned, "Banned should be false if banlist disabled")
	assert.Equal(t, "", reason, "Reason should be empty if banlist disabled")
}

func TestIsBannedNotBanned(t *testing.T) {
	config.C.YP.EnableBanlist = true
	banlist = defaultBans
	banned, reason := IsBanned("example.com", "8.8.8.8")
	assert.Equal(t, false, banned, "Banned should be false if not banned")
	assert.Equal(t, "", reason, "Reason should be empty if not banned")
}

func TestIsBannedBannedIP(t *testing.T) {
	config.C.YP.EnableBanlist = true
	banlist = defaultBans
	t.Logf("%+v", defaultBans)
	banned, reason := IsBanned("example.com", "10.0.0.1")
	assert.Equal(t, true, banned, "Banned should be true if banned")
	assert.Equal(t, "Private IP address range", reason, "Reason should not be empty if banned")
}

func TestIsBannedBannedHost(t *testing.T) {
	config.C.YP.EnableBanlist = true
	banlist = defaultBans
	banned, reason := IsBanned("test.local", "8.8.8.8")
	assert.Equal(t, true, banned, "Banned should be true if banned")
	assert.Equal(t, "Private IP address range", reason, "Reason should not be empty if banned")
}
