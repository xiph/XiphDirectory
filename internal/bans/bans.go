// Package bans provides the XiphDirectory banlist implementation
package bans

import (
	"bytes"
	"errors"
	"io/ioutil"
	"net"
	"os"
	"regexp"

	"github.com/BurntSushi/toml"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"gitlab.xiph.org/xiph/XiphDirectory/internal/config"
)

type ban struct {
	Reason          string
	IPs             []string
	IPNets          []*net.IPNet `toml:"-"`
	HostPatterns    []string
	HostPatternsReg []*regexp.Regexp `toml:"-"`
}

type bl struct {
	Bans []ban
}

var banlist bl

var slog zerolog.Logger

func processBanlist(banlist bl) (bl, []error) {
	var errs []error
	for b := range banlist.Bans {
		banlist.Bans[b].HostPatternsReg = nil
		banlist.Bans[b].IPNets = nil
		for h := range banlist.Bans[b].HostPatterns {
			reg, err := regexp.Compile(banlist.Bans[b].HostPatterns[h])
			if err != nil {
				slog.Err(err).Send()
				errs = append(errs, err)
			}
			banlist.Bans[b].HostPatternsReg = append(banlist.Bans[b].HostPatternsReg, reg)
		}
		for i := range banlist.Bans[b].IPs {
			_, ip, err := net.ParseCIDR(banlist.Bans[b].IPs[i])
			if err != nil {
				slog.Err(err).Send()
				errs = append(errs, err)
			}
			banlist.Bans[b].IPNets = append(banlist.Bans[b].IPNets, ip)
		}
	}
	return banlist, errs
}

// LoadDefaults puts default values into Banlist
func LoadDefaults() {
	banlist.Bans = []ban{}
	banlist.Bans = append(banlist.Bans, ban{
		Reason:       "Private IP address range",
		IPs:          []string{"127.0.0.0/8", "10.0.0.0/8", "172.16.0.0/12", "192.168.0.0/16", "169.254.0.0/16"},
		HostPatterns: []string{".*\\.local"},
	})
}

// LoadBanlist loads the banlist found at the given path
func LoadBanlist(path string) error {
	slog = log.With().Str("component", "banlist").Logger()
	if !config.C.YP.EnableBanlist {
		return nil
	}
	LoadDefaults()
	_, err := toml.DecodeFile(path, &banlist)
	if errors.Is(err, os.ErrNotExist) {
		slog.Warn().Str("path", path).Msg("Could not find file, using defaults!")
		err = WriteBanlist(path)
		if err != nil {
			return err
		}
	} else if err != nil {
		slog.Error().Str("path", path).Err(err).Send()
		return err
	}
	var errs []error
	banlist, errs = processBanlist(banlist)
	slog.Info().Errs("errors", errs).Str("path", path).Msg("Banlist loaded")
	return err
}

// WriteBanlist writes the banlist to the given path
func WriteBanlist(path string) error {
	buf := new(bytes.Buffer)
	err := toml.NewEncoder(buf).Encode(banlist)
	if err != nil {
		slog.Err(err).Send()
		return err
	}
	err = ioutil.WriteFile(path, buf.Bytes(), 0600)
	if err != nil {
		slog.Err(err).Send()
		return err
	}
	slog.Info().Str("path", path).Msg("Wrote banlist")
	return err
}

// IsBanned checks if IP or Hostname are banned
func IsBanned(hostname string, ip string) (bool, string) {
	if !config.C.YP.EnableBanlist {
		return false, ""
	}
	ipAddr := net.ParseIP(ip)
	for _, ban := range banlist.Bans {
		for _, bannedIPNet := range ban.IPNets {
			if bannedIPNet.Contains(ipAddr) {
				slog.Debug().Str("reason", ban.Reason).IPPrefix("banned_net", *bannedIPNet).Msg("Server banned")
				return true, ban.Reason
			}
		}
		for _, bannedHostnamePatternRegex := range ban.HostPatternsReg {
			match := bannedHostnamePatternRegex.MatchString(hostname)
			if match {
				slog.Debug().Str("reason", ban.Reason).Str("banned_hostname", hostname).Msg("Server banned")
				return true, ban.Reason
			}
		}
	}
	return false, ""
}

// ReloadBanlist reloads the banlist
func ReloadBanlist(path string) {
	if !config.C.YP.EnableBanlist {
		slog.Info().Msg("Banlist disabled, not reloading it")
		return
	}
	slog.Info().Msg("Reloading banlist")
	err := LoadBanlist(path)
	if err != nil {
		slog.Error().Err(err).Msg("Error while reloading the banlist")
	}
}
