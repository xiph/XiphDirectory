// Package rotate provides a roatateable writer for the logs
package rotate

import (
	"os"
	"sync"

	"github.com/rs/zerolog/log"
)

// Writer implements the writer interface
type Writer struct {
	lock     sync.Mutex
	fp       *os.File
	filename string
}

// New makes a new RotateWriter with the given config fields
func New(path string) (*Writer, error) {
	w := &Writer{filename: path}
	err := w.Rotate()
	return w, err
}

// Write satisfies the io.Writer interface.
func (w *Writer) Write(output []byte) (int, error) {
	w.lock.Lock()
	defer w.lock.Unlock()
	return w.fp.Write(output)
}

// Rotate closes and then reopens/creates the log file
func (w *Writer) Rotate() (err error) {
	w.lock.Lock()
	defer w.lock.Unlock()

	// Close existing file if open
	if w.fp != nil && w.fp != os.Stderr {
		_ = w.fp.Close()
		w.fp = nil
	}

	// Create the file
	w.fp, err = os.Create(w.filename)
	if err != nil {
		log.Error().Err(err).Msg("Cannot create log file, using stderr!")
		w.fp = os.Stderr
	}
	return
}
