package web

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	uuid "github.com/satori/go.uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.xiph.org/xiph/XiphDirectory/internal/config"
	"gitlab.xiph.org/xiph/XiphDirectory/internal/database"
)

// frontend.go
func TestGetBestServerForStream50(t *testing.T) {
	var Stream database.Stream

	bestServer := database.Server{
		ID:           uuid.NewV4(),
		Listeners:    50,
		MaxListeners: 100,
		ListenURL:    "thebest",
	}

	Stream.Servers = append(Stream.Servers, database.Server{
		ID:           uuid.NewV4(),
		Listeners:    95,
		MaxListeners: 100,
		ListenURL:    "nearlyfull",
	})

	Stream.Servers = append(Stream.Servers, database.Server{
		ID:           uuid.NewV4(),
		Listeners:    25,
		MaxListeners: 25,
		ListenURL:    "fullserver",
	})

	Stream.Servers = append(Stream.Servers, bestServer)

	getBestServerForStream(&Stream)

	assert.Equal(t, bestServer.ListenURL, Stream.ListenURL, "ListenURL should be the one from the best server")
	assert.Equal(t, float32(0.5), Stream.ListenURLLoad, "ListenURLLoad should be 50%")
}

func TestGetBestServerForStreamUnlimited(t *testing.T) {
	var Stream database.Stream

	bestServer := database.Server{
		ID:           uuid.NewV4(),
		Listeners:    50,
		MaxListeners: 0,
		ListenURL:    "thebest",
	}

	Stream.Servers = append(Stream.Servers, database.Server{
		ID:           uuid.NewV4(),
		Listeners:    95,
		MaxListeners: 100,
		ListenURL:    "nearlyfull",
	})

	Stream.Servers = append(Stream.Servers, database.Server{
		ID:           uuid.NewV4(),
		Listeners:    25,
		MaxListeners: 25,
		ListenURL:    "fullserver",
	})

	Stream.Servers = append(Stream.Servers, bestServer)

	getBestServerForStream(&Stream)

	assert.Equal(t, bestServer.ListenURL, Stream.ListenURL, "ListenURL should be the one from the best server")
	assert.Equal(t, float32(0.0), Stream.ListenURLLoad, "ListenURLLoad should be 0% (unlimited listeners)")
}

// yp.go
func TestListenURLMatchesIPDisabled(t *testing.T) {
	config.C.YP.VerifyListenURL = false
	result := listenURLMatchesIP("127.0.0.1", "http://example.com/listen")
	assert.Equal(t, true, result, "Should always be true if verification is disabled")
}

func TestListenURLMatchesIPSuccessfulV4(t *testing.T) {
	config.C.YP.VerifyListenURL = true
	result := listenURLMatchesIP("93.184.216.34", "http://example.com/listen")
	assert.Equal(t, true, result, "93.184.216.34 should match the example.com hostname")
}

func TestListenURLMatchesIPSuccessfulV6(t *testing.T) {
	config.C.YP.VerifyListenURL = true
	result := listenURLMatchesIP("2606:2800:220:1:248:1893:25c8:1946", "http://example.com/listen")
	assert.Equal(t, true, result, "2606:2800:220:1:248:1893:25c8:1946 should match the example.com hostname")
}

func TestListenURLMatchesIPFail(t *testing.T) {
	config.C.YP.VerifyListenURL = true
	result := listenURLMatchesIP("127.0.0.1", "http://example.com/listen")
	assert.Equal(t, false, result, "127.0.0.1 should not match the example.com hostname")
}

func TestGetCodecForMIMEUnknown(t *testing.T) {
	result := ypGetCodecForMIME("unknown/unknown")
	assert.Equal(t, "Other", result, "The returned Codec on unknown media type should be 'Other'")
}

func TestGetCodecForMIMESimple(t *testing.T) {
	result := ypGetCodecForMIME("audio/aac")
	assert.Equal(t, "AAC", result, "The returned Codec for audio/aac should be 'AAC'")
}

func TestGetCodecForMIMEUpper(t *testing.T) {
	result := ypGetCodecForMIME("AUDIO/OPUS")
	assert.Equal(t, "Opus", result, "The returned Codec for AUDIO/OPUS should be 'Opus'")
}

func TestGetCodecForMIMEWithParams(t *testing.T) {
	result := ypGetCodecForMIME("audio/ogg;codecs=vorbis")
	assert.Equal(t, "Vorbis", result, "The returned Codec for audio/ogg;codecs=vorbis should be 'Vorbis'")
}

func TestGetCodecForCodecUpper(t *testing.T) {
	result := ypGetCodecForCodec("AAC PLUS")
	assert.Equal(t, "AAC+", result, "The returned Codec for AAC PLUS should be 'AAC+'")
}

func TestGetCodecForCodecUnknown(t *testing.T) {
	result := ypGetCodecForCodec("Unknown CODEC")
	assert.Equal(t, "Other", result, "The returned Codec for an unknown codec should be 'Other'")
}

func TestGenreTrimFuncLetter(t *testing.T) {
	result := ypGenreTrimFunc('a')
	assert.Equal(t, false, result, "Function should not trim letters")
}

func TestGenreTrimFuncNumber(t *testing.T) {
	result := ypGenreTrimFunc('7')
	assert.Equal(t, false, result, "Function should not trim numbers")
}

func TestGenreTrimFuncOther(t *testing.T) {
	result := ypGenreTrimFunc(')')
	assert.Equal(t, true, result, "Function should trim bracets")
	result = ypGenreTrimFunc('+')
	assert.Equal(t, true, result, "Function should trim signs")
	result = ypGenreTrimFunc('😀')
	assert.Equal(t, true, result, "Function should trim emoji")
}

func TestResponseHeadersCaseSensitive(t *testing.T) {
	gin.SetMode(gin.TestMode)
	r := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(r)
	ypSetHeader(c, "YPResponse", "0")
	ypSetHeader(c, "Ypmessage", "I won't be there")
	ypSetHeader(c, "YPMessage", "But I will!")
	ypSetHeader(c, "YPFoo", "I won't be there either")
	ypSetHeader(c, "YPFoo", "")
	result := r.Header()

	assert.Equal(t, http.Header{
		"YPResponse": []string{"0"},
		"YPMessage":  []string{"But I will!"},
	}, result, "Case-sensitive headers should match")
}
