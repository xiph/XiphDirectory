package web

import (
	"encoding/xml"
	"html/template"
	"math"
	"net/http"
	"net/url"
	"sync"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"gitlab.xiph.org/xiph/XiphDirectory/internal/config"
	"gitlab.xiph.org/xiph/XiphDirectory/internal/database"
)

type directory struct {
	XMLName xml.Name          `xml:"directory"`
	Entry   []database.Stream `xml:"entry"`
}

type ypXMLResponse struct {
	Response   []byte
	Error      error
	LastUpdate time.Time
	mux        sync.RWMutex
}

var ypXML ypXMLResponse

func marshalYpXML(streams []database.Stream) {
	ypXML.mux.Lock()
	dir := directory{Entry: streams}
	ypXML.Response, ypXML.Error = xml.Marshal(&dir)
	ypXML.LastUpdate = time.Now()
	ypXML.mux.Unlock()
}

func checkYpXMLNeedsUpdate() bool {
	ypXML.mux.RLock()
	defer ypXML.mux.RUnlock()
	return ypXML.LastUpdate.Before(time.Now().Add(-time.Duration(config.C.Web.XMLCacheTime) * time.Second))
}

func getBestServerForStream(stream *database.Stream) {
	stream.ListenURLLoad = (float32)(math.Inf(1))
	for _, Server := range stream.Servers {
		var load float32
		if Server.MaxListeners == 0 {
			load = 0.0
		} else {
			load = float32(Server.Listeners) / float32(Server.MaxListeners)
		}
		if load < stream.ListenURLLoad {
			stream.ListenURLLoad = load
			stream.ListenURL = Server.ListenURL
			stream.NowPlaying = Server.NowPlaying
		}
	}
}

func getYpXML(c *gin.Context) {
	if checkYpXMLNeedsUpdate() {
		var Streams []database.Stream
		database.Db.Preload("Servers").Preload("Genres").Find(&Streams)
		for i := range Streams {
			getBestServerForStream(&Streams[i])
			for _, Genre := range Streams[i].Genres {
				Streams[i].Genre += (Genre.Name + " ")
			}
			if Streams[i].Genre != "" {
				Streams[i].Genre = Streams[i].Genre[0 : len(Streams[i].Genre)-1]
			}
		}
		marshalYpXML(Streams)
	}
	ypXML.mux.RLock()
	defer ypXML.mux.RUnlock()
	c.Data(http.StatusOK, "application/xml", ypXML.Response)
}

func getIndex(c *gin.Context) {
	var Streams []database.Stream
	database.Db.Preload("Genres").Preload("Codecs").Preload("Servers").Order("RANDOM()").Limit(config.C.Web.PageSize).Find(&Streams)
	for i := range Streams {
		getBestServerForStream(&Streams[i])
	}
	c.HTML(http.StatusOK, "index.page.tmpl", gin.H{"data": Streams, "title": "Index"})
}

func getGenres(c *gin.Context) {
	var Genres []database.Genre
	database.Db.Order("stream_count DESC").Find(&Genres)
	c.HTML(http.StatusOK, "genres.page.tmpl", gin.H{"data": Genres, "title": "All Genres"})
}

// nolint dupl
func getGenre(c *gin.Context) {
	var Genre database.Genre
	if database.Db.First(&Genre, "name = ?", c.Param("name")).RecordNotFound() {
		c.HTML(http.StatusNotFound, "error.page.tmpl", gin.H{"error": "Genre not fround", "title": "Genre not found"})
		return
	}

	stmt := database.Db.Model(&Genre)
	stmt, flipped := database.Paginate(stmt, c.Query("cursor"), "Listeners", "DESC")
	stmt.Related(&Genre.Streams, "Streams")
	if flipped {
		database.ReverseSlice(Genre.Streams)
	}

	cursors := database.GenerateCursor(Genre.Streams, database.Db.Model(&Genre), true, "Listeners", "DESC")

	paramsNext := url.Values{}
	paramsNext.Add("cursor", cursors.Next)
	paramsPrev := url.Values{}
	paramsPrev.Add("cursor", cursors.Prev)

	for i := range Genre.Streams {
		database.Db.Model(&Genre.Streams[i]).Association("Servers").Find(&Genre.Streams[i].Servers)
		database.Db.Model(&Genre.Streams[i]).Association("Codecs").Find(&Genre.Streams[i].Codecs)
		database.Db.Model(&Genre.Streams[i]).Association("Genres").Find(&Genre.Streams[i].Genres)
		getBestServerForStream(&Genre.Streams[i])
	}

	c.HTML(http.StatusOK, "genre.page.tmpl", gin.H{"data": Genre, "next": template.URL(paramsNext.Encode()), "prev": template.URL(paramsPrev.Encode()), "cursors": cursors, "title": "Genre " + Genre.Name})
}

func getCodecs(c *gin.Context) {
	var Codecs []database.Codec
	database.Db.Order("stream_count DESC").Find(&Codecs)
	c.HTML(http.StatusOK, "codecs.page.tmpl", gin.H{"data": Codecs, "title": "All Codecs"})
}

// nolint dupl
func getCodec(c *gin.Context) {
	var Codec database.Codec
	if database.Db.First(&Codec, "name = ?", c.Param("name")).RecordNotFound() {
		c.HTML(http.StatusNotFound, "error.page.tmpl", gin.H{"error": "Codec not found", "title": "Codec not found"})
		return
	}

	stmt := database.Db.Model(&Codec)
	stmt, flipped := database.Paginate(stmt, c.Query("cursor"), "Listeners", "DESC")
	stmt.Related(&Codec.Streams, "Streams")
	if flipped {
		database.ReverseSlice(Codec.Streams)
	}

	cursors := database.GenerateCursor(Codec.Streams, database.Db.Model(&Codec), true, "Listeners", "DESC")

	paramsNext := url.Values{}
	paramsNext.Add("cursor", cursors.Next)
	paramsPrev := url.Values{}
	paramsPrev.Add("cursor", cursors.Prev)

	for i := range Codec.Streams {
		database.Db.Model(&Codec.Streams[i]).Association("Servers").Find(&Codec.Streams[i].Servers)
		database.Db.Model(&Codec.Streams[i]).Association("Codecs").Find(&Codec.Streams[i].Codecs)
		database.Db.Model(&Codec.Streams[i]).Association("Genres").Find(&Codec.Streams[i].Genres)
		getBestServerForStream(&Codec.Streams[i])
	}

	c.HTML(http.StatusOK, "codec.page.tmpl", gin.H{"data": Codec, "next": template.URL(paramsNext.Encode()), "prev": template.URL(paramsPrev.Encode()), "cursors": cursors, "title": "Codec " + Codec.Name})
}

func doSearchQuery(c *gin.Context, streams *[]database.Stream, genres []string, codecs []string, query string) *gorm.DB {
	stmt := database.Db
	stmt2 := database.Db.Model(streams)
	if len(genres) != 0 {
		stmt = stmt.Joins("JOIN stream_genres ON stream_genres.stream_id = streams.id AND stream_genres.genre_id IN (?)", genres)
		stmt2 = stmt2.Joins("JOIN stream_genres ON stream_genres.stream_id = streams.id AND stream_genres.genre_id IN (?)", genres)
	}
	if len(codecs) != 0 {
		stmt = stmt.Joins("JOIN stream_codecs ON stream_codecs.stream_id = streams.id AND stream_codecs.codec_id IN (?)", codecs)
		stmt2 = stmt2.Joins("JOIN stream_codecs ON stream_codecs.stream_id = streams.id AND stream_codecs.codec_id IN (?)", codecs)
	}
	if query != "" {
		stmt = stmt.Where("stream_name ILIKE ? OR description ILIKE ?", "%"+query+"%", "%"+query+"%")
		stmt2 = stmt2.Where("stream_name ILIKE ? OR description ILIKE ?", "%"+query+"%", "%"+query+"%")
	}
	stmt, flipped := database.Paginate(stmt.Group("id"), c.Query("cursor"), "CreatedAt", "ASC")
	stmt.Preload("Servers").Preload("Codecs").Preload("Genres").Find(streams)
	if flipped {
		database.ReverseSlice(*streams)
	}
	return stmt2
}

func getSearch(c *gin.Context) {
	var Streams []database.Stream
	query := c.Query("q")
	genres := c.QueryArray("filter_genres")
	codecs := c.QueryArray("filter_codecs")
	if query == "" && len(genres) == 0 && len(codecs) == 0 {
		var Genres []database.Genre
		var Codecs []database.Codec
		database.Db.Order("name ASC").Find(&Genres)
		database.Db.Order("name ASC").Find(&Codecs)
		c.HTML(http.StatusOK, "search_form.page.tmpl", gin.H{"data": gin.H{"genres": Genres, "codecs": Codecs}, "title": "Search"})
		return
	}

	stmt2 := doSearchQuery(c, &Streams, genres, codecs, query)

	for i := range Streams {
		getBestServerForStream(&Streams[i])
	}
	cursors := database.GenerateCursor(Streams, stmt2.Group("id"), false, "CreatedAt", "ASC")

	paramsNext := url.Values{}
	paramsPrev := url.Values{}
	for _, genre := range genres {
		paramsNext.Add("filter_genres", genre)
		paramsPrev.Add("filter_genres", genre)
	}
	for _, codec := range codecs {
		paramsNext.Add("filter_codecs", codec)
		paramsPrev.Add("filter_codecs", codec)
	}
	if query != "" {
		paramsNext.Add("q", query)
		paramsPrev.Add("q", query)
	}

	paramsNext.Add("cursor", cursors.Next)
	paramsPrev.Add("cursor", cursors.Prev)

	// #nosec G203
	c.HTML(http.StatusOK, "search.page.tmpl", gin.H{"data": Streams, "next": template.URL(paramsNext.Encode()), "prev": template.URL(paramsPrev.Encode()), "cursors": cursors, "title": "Search"})
}
