package web

import (
	"crypto/sha256"
	"encoding/hex"
	"mime"
	"net"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"
	"unicode"
	"unicode/utf8"

	"github.com/gin-gonic/gin"
	"github.com/lib/pq"
	"github.com/rs/zerolog"
	uuid "github.com/satori/go.uuid"
	"gitlab.xiph.org/xiph/XiphDirectory/internal/bans"
	"gitlab.xiph.org/xiph/XiphDirectory/internal/config"
	"gitlab.xiph.org/xiph/XiphDirectory/internal/database"
)

const ypErr = "0"
const ypOki = "1"

var yplog zerolog.Logger

type ypRes struct {
	SID       uuid.UUID
	Status    int
	Message   string
	Err       error
	ListenURL string
}

// Set a HTTP header to the given value but preserve
// the case of the header key. This is needed for very
// old Icecast versions and current versions of Icecast-kh
// and the AzuraCast icecast-kh-ac fork
func ypSetHeader(c *gin.Context, key string, value string) {
	w := c.Writer
	headermap := w.Header()
	for k := range headermap {
		if strings.EqualFold(key, k) {
			delete(headermap, k)
		}
	}
	if value != "" {
		headermap[key] = []string{value}
	}
}

func ypResponse(c *gin.Context, res ypRes) {
	var logevent *zerolog.Event
	var ypStatus string

	if res.Status/100 <= 2 {
		logevent = yplog.Trace()
		ypStatus = ypOki
	} else {
		if res.Err == nil {
			logevent = yplog.Info()
		} else {
			logevent = yplog.Warn()
		}
		ypStatus = ypErr
	}

	ypSetHeader(c, "YPResponse", ypStatus)
	logevent = logevent.Str("yp_response", ypStatus)
	ypSetHeader(c, "YPMessage", res.Message)
	logevent = logevent.Str("yp_message", res.Message)

	if res.SID == uuid.Nil || ypStatus != ypOki {
		ypSetHeader(c, "SID", "-1")
	} else {
		ypSetHeader(c, "SID", res.SID.String())
	}
	logevent = logevent.Str("sid", res.SID.String())

	logevent = logevent.Err(res.Err).Str("client_ip", c.ClientIP()).Str("listen_url", res.ListenURL)

	logevent.Msg("YP Response")
	c.Data(res.Status, "", nil)
}

func listenURLMatchesIP(ip string, listenURL string) bool {
	if !config.C.YP.VerifyListenURL {
		return true
	}
	url, _ := url.Parse(listenURL)
	ips, _ := net.LookupIP(url.Hostname())

	for _, b := range ips {
		if b.String() == ip {
			return true
		}
	}
	yplog.Debug().Str("hostname", url.Hostname()).Interface("dns_ips", ips).Str("client_ip", ip).Msg("Hostname does not match IP")
	return false
}

func ypGetCodecForMIME(m string) string {
	med, params, err := mime.ParseMediaType(m)
	if err != nil {
		yplog.Debug().Err(err).Msg("Error parsing the media type")
	}
	if params["codecs"] != "" {
		return ypGetCodecForCodec(params["codecs"])
	}
	switch med {
	case "audio/flac", "audio/x-flac":
		return "FLAC"
	case "audio/opus", "application/ogg+opus":
		return "Opus"
	case "audio/ogg", "application/ogg", "application/ogg+vorbis", "application/x-ogg":
		return "Vorbis"
	case "application/mp3", "audio/x-mpeg", "audio/mpeg", "audio/mpa", "audio/mpa-robust", "audio/mpeg3", "audio/x-mpeg-3", "application/mpeg":
		return "MP3"
	case "audio/aac", "audio/mp4":
		return "AAC"
	case "audio/aacp", "application/aacp":
		return "AAC+"
	case "video/webm", "audio/webm":
		return "WebM"
	case "video/ogg", "application/ogg+theora":
		return "Theora"
	case "video/nsv":
		return "NSV"
	case "video/mp2t":
		return "MPEG-TS"
	default:
		yplog.Debug().Str("mediatype_supplied", m).Str("mediatype_parsed", med).Interface("params", params).Msg("Does not match predefined media types, trying to parse as codec")
		return ypGetCodecForCodec(m)
	}
}

func ypGetCodecForCodec(codec string) string {
	codec = strings.Trim(codec, " \r\n;")
	codec = strings.ToLower(codec)
	switch codec {
	case "opus":
		return "Opus"
	case "vorbis", "skeleton":
		return "Vorbis"
	case "mp3", "mp 3", "mp3 audio", "lame", "mpeg":
		return "MP3"
	case "aac", "aac-lc":
		return "AAC"
	case "aacp", "aac plus", "aac+", "aac +":
		return "AAC+"
	case "webm", "vp9", "vp8":
		return "WebM"
	case "theora":
		return "Theora"
	case "nsv":
		return "NSV"
	case "pcm":
		return "PCM"
	default:
		yplog.Debug().Str("codec", codec).Msg("Does not match predefined codecs")
		return "Other"
	}
}

func ypPostForm(c *gin.Context, key string) string {
	return strings.ToValidUTF8(c.PostForm(key), string(utf8.RuneError))
}

func ypGenreTrimFunc(r rune) bool {
	return !unicode.IsLetter(r) && !unicode.IsNumber(r)
}

func handleServerListenURL(c *gin.Context, server database.Server) bool {
	if !listenURLMatchesIP(server.IP, server.ListenURL) {
		client := http.Client{
			Timeout: time.Duration(5) * time.Second,
		}
		req, _ := http.NewRequest("GET", server.ListenURL, nil)
		req.Header.Add("User-Agent", "XiphDirectory/1.0.0")
		resp, err := client.Do(req)
		if err != nil {
			ypResponse(c, ypRes{
				SID:       uuid.Nil,
				Status:    http.StatusForbidden,
				Message:   "Host does not match IP, GET-test was also negative",
				Err:       err,
				ListenURL: server.ListenURL,
			})
			return false
		}
		err = resp.Body.Close()
		if err != nil {
			yplog.Error().Err(err).Msg("Can not close response body")
		}
		if resp.StatusCode != 200 {
			ypResponse(c, ypRes{
				SID:       uuid.Nil,
				Status:    http.StatusForbidden,
				Message:   "Host does not match IP, GET-test returned non-200 status code",
				Err:       nil,
				ListenURL: server.ListenURL,
			})
			return false
		}
		ct := resp.Header.Get("Content-Type")
		if !strings.Contains(ct, "application") && !strings.Contains(ct, "audio") && !strings.Contains(ct, "video") {
			yplog.Debug().Str("content_type", ct).Msg("Content-Type not application/audio/video")
			ypResponse(c, ypRes{
				SID:       uuid.Nil,
				Status:    http.StatusForbidden,
				Message:   "Host does not match IP, GET-test returned wrong content type",
				Err:       nil,
				ListenURL: server.ListenURL,
			})
			return false
		}
		yplog.Info().Str("listen_url", server.ListenURL).Str("client_ip", c.ClientIP()).Msg("Server failed DNS test, but passed GET test")
	}
	return true
}

func doCpAdd(c *gin.Context, stream *database.Stream, cphash string) bool {
	sn := ypPostForm(c, "sn")
	listenurl := ypPostForm(c, "listenurl")
	if !database.Db.Where("stream_name = ? AND cluster_pass = ?", sn, cphash).Preload("Servers").First(stream).RecordNotFound() {
		var Server database.Server
		Server.LastTouch = time.Now()
		Server.ListenURL = listenurl
		Server.IP = c.ClientIP()

		if !handleServerListenURL(c, Server) {
			return false
		}

		if err := database.Db.Model(stream).Association("Servers").Append(&Server).Error; err != nil {
			if err, ok := err.(*pq.Error); ok && err.Code.Name() == "unique_violation" {
				ypResponse(c, ypRes{
					SID:       uuid.Nil,
					Status:    http.StatusConflict,
					Message:   "Duplicate stream",
					Err:       err,
					ListenURL: listenurl,
				})
				return false
			}
			ypResponse(c, ypRes{
				SID:       uuid.Nil,
				Status:    http.StatusInternalServerError,
				Message:   "Error while adding server to database",
				Err:       err,
				ListenURL: listenurl,
			})
			return false
		}

		ypSetHeader(c, "TouchFreq", strconv.Itoa(config.C.YP.TouchFreq))
		ypResponse(c, ypRes{
			SID:       Server.ID,
			Status:    http.StatusCreated,
			Message:   "Created successfully",
			Err:       nil,
			ListenURL: listenurl,
		})
		return false
	}
	return true
}

func getMandatoryAddParams(c *gin.Context) (string, string, string, string, bool) {
	t := c.PostForm("type")
	b := c.PostForm("b")
	sn := ypPostForm(c, "sn")
	listenurl := ypPostForm(c, "listenurl")
	if sn == "" || t == "" || listenurl == "" {
		ypResponse(c, ypRes{
			SID:       uuid.Nil,
			Status:    http.StatusUnprocessableEntity,
			Message:   "Missing mandatory parameter",
			Err:       nil,
			ListenURL: listenurl,
		})
		return t, b, sn, listenurl, false
	}

	u, err := url.Parse(listenurl)
	if err != nil {
		ypResponse(c, ypRes{
			SID:       uuid.Nil,
			Status:    http.StatusUnprocessableEntity,
			Message:   "Can not parse listenurl",
			Err:       err,
			ListenURL: listenurl,
		})
		return t, b, sn, listenurl, false
	}
	if banned, reason := bans.IsBanned(u.Hostname(), c.ClientIP()); banned {
		ypResponse(c, ypRes{
			SID:       uuid.Nil,
			Status:    http.StatusForbidden,
			Message:   reason,
			Err:       nil,
			ListenURL: listenurl,
		})
		return t, b, sn, listenurl, false
	}
	return t, b, sn, listenurl, true
}

func brokenBodyWorkaround(c *gin.Context, stream *database.Stream) bool {
	// Handle broken body
	m := make(map[string]string)
	for key := range c.Request.PostForm {
		if !strings.Contains(key, "=") {
			continue
		}
		entries := strings.Split(key, ";")
		for _, e := range entries {
			parts := strings.Split(e, "=")
			m[parts[0]] = strings.Trim(parts[1], "\r\n ")
		}
		break
	}
	if stream.Bitrate == 0 {
		stream.Bitrate, _ = strconv.Atoi(m["bitrate"])
	}
	stream.Samplerate, _ = strconv.Atoi(m["samplerate"])
	stream.Channels, _ = strconv.Atoi(m["channels"])
	qualstr, _ := url.QueryUnescape(m["quality"])
	qual, errfl := strconv.ParseFloat(qualstr, 32)
	if errfl != nil {
		qual = -1.0
	}
	stream.Quality = float32(qual)
	return true
}

func parseAddBody(c *gin.Context, stream *database.Stream) bool {
	if c.PostForm("samplerate") != "" || c.PostForm("channels") != "" || c.PostForm("quality") != "" {
		// Handle fixed body
		if stream.Bitrate == 0 {
			stream.Bitrate, _ = strconv.Atoi(c.PostForm("bitrate"))
		}
		stream.Samplerate, _ = strconv.Atoi(c.PostForm("samplerate"))
		stream.Channels, _ = strconv.Atoi(c.PostForm("channels"))
		qual, errfl := strconv.ParseFloat(c.PostForm("quality"), 32)
		if errfl != nil {
			qual = -1.0
		}
		stream.Quality = float32(qual)
	} else if !brokenBodyWorkaround(c, stream) {
		return false
	}
	return true
}

func getStreamCodecs(c *gin.Context, stream *database.Stream) bool {
	if c.PostForm("stype") == "" || c.PostForm("stype") == "/" {
		codec := ypGetCodecForMIME(stream.StreamType)
		var Codec database.Codec
		database.Db.FirstOrInit(&Codec, database.Codec{Name: codec})
		Codec.StreamCount++
		stream.Codecs = append(stream.Codecs, Codec)
	} else {
		codecs := strings.Split(c.PostForm("stype"), "/")
		for _, codec := range codecs {
			codec = ypGetCodecForCodec(codec)
			var Codec database.Codec
			database.Db.FirstOrInit(&Codec, database.Codec{Name: codec})
			Codec.StreamCount++
			stream.Codecs = append(stream.Codecs, Codec)
		}
	}
	return true
}

func getStreamGenres(c *gin.Context, stream *database.Stream) bool {
	g := ypPostForm(c, "genre")
	var genres []string
	switch {
	case strings.ContainsRune(g, ','):
		genres = strings.Split(g, ",")
	case strings.ContainsRune(g, '/'):
		genres = strings.Split(g, "/")
	default:
		genres = strings.Split(g, " ")
	}
	if g != "" {
		for _, genre := range genres {
			genre = strings.TrimFunc(genre, ypGenreTrimFunc)
			if genre == "" {
				continue
			}
			var Genre database.Genre
			database.Db.FirstOrInit(&Genre, database.Genre{Name: genre})
			Genre.StreamCount++
			stream.Genres = append(stream.Genres, Genre)
		}
	}
	if len(stream.Genres) == 0 {
		var Genre database.Genre
		database.Db.FirstOrInit(&Genre, database.Genre{Name: "Other"})
		Genre.StreamCount++
		stream.Genres = append(stream.Genres, Genre)
	}
	return true
}

func doStreamDatabaseAdd(c *gin.Context, stream *database.Stream) bool {
	if err := database.Db.Create(stream).Error; err != nil {
		if err, ok := err.(*pq.Error); ok && err.Code.Name() == "unique_violation" {
			ypResponse(c, ypRes{
				SID:       uuid.Nil,
				Status:    http.StatusConflict,
				Message:   "Duplicate stream",
				Err:       err,
				ListenURL: stream.Servers[0].ListenURL,
			})
			return false
		}
		ypResponse(c, ypRes{
			SID:       uuid.Nil,
			Status:    http.StatusInternalServerError,
			Message:   "Database error while creating stream",
			Err:       err,
			ListenURL: stream.Servers[0].ListenURL,
		})
		return false
	}
	return true
}

func hashClusterPassword(c *gin.Context) (string, string) {
	cp := c.PostForm("cpswd")
	hash := sha256.Sum256([]byte(cp))
	return cp, hex.EncodeToString(hash[:])
}

func ypAdd(c *gin.Context) {
	t, b, sn, listenurl, cont := getMandatoryAddParams(c)
	if !cont {
		return
	}

	var Stream database.Stream

	cp, cphash := hashClusterPassword(c)
	if cp != "" {
		if !doCpAdd(c, &Stream, cphash) {
			return
		}
	}

	Stream.StreamName = sn
	Stream.StreamType = t
	Stream.Bitrate, _ = strconv.Atoi(b)

	if !parseAddBody(c, &Stream) {
		return
	}

	if cp != "" {
		Stream.ClusterPass = cphash
	}
	Stream.Description = ypPostForm(c, "desc")
	Stream.URL = ypPostForm(c, "url")

	var Server database.Server
	Server.LastTouch = time.Now()
	Server.ListenURL = listenurl
	Server.IP = c.ClientIP()

	if !handleServerListenURL(c, Server) {
		return
	}

	Stream.Servers = append(Stream.Servers, Server)

	if !getStreamCodecs(c, &Stream) {
		return
	}

	if !getStreamGenres(c, &Stream) {
		return
	}

	if !doStreamDatabaseAdd(c, &Stream) {
		return
	}

	ypSetHeader(c, "TouchFreq", strconv.Itoa(config.C.YP.TouchFreq))
	ypResponse(c, ypRes{
		SID:       Stream.Servers[0].ID,
		Status:    http.StatusCreated,
		Message:   "Created successfully",
		Err:       nil,
		ListenURL: Stream.Servers[0].ListenURL,
	})
}

func doTouchServerUpdate(c *gin.Context, server *database.Server) bool {
	if st := ypPostForm(c, "st"); st != "" {
		server.NowPlaying = st
	}

	if listeners := c.PostForm("listeners"); listeners != "" {
		listencount, _ := strconv.ParseUint(listeners, 10, 32)
		server.Listeners = uint(listencount)
	}

	if maxListeners := c.PostForm("max_listeners"); maxListeners != "" {
		server.MaxListeners, _ = strconv.Atoi(maxListeners)
	}

	server.LastTouch = time.Now()
	res := database.Db.Model(database.Server{}).Where("id = ?", server.ID).Update(server)
	if err := res.Error; err != nil {
		ypResponse(c, ypRes{
			SID:       server.ID,
			Status:    http.StatusInternalServerError,
			Message:   "Database error while updating stream",
			Err:       err,
			ListenURL: "",
		})
		return false
	}
	if res.RowsAffected == 0 {
		ypResponse(c, ypRes{
			SID:       server.ID,
			Status:    http.StatusNotFound,
			Message:   "SID not found",
			Err:       nil,
			ListenURL: "",
		})
		return false
	}
	return true
}

func ypTouch(c *gin.Context) {
	sid := c.PostForm("sid")
	if sid == "" {
		ypResponse(c, ypRes{
			SID:       uuid.Nil,
			Status:    http.StatusUnprocessableEntity,
			Message:   "No SID",
			Err:       nil,
			ListenURL: "",
		})
		return
	}

	var Server database.Server
	var err error
	Server.ID, err = uuid.FromString(sid)
	if err != nil {
		ypResponse(c, ypRes{
			SID:       uuid.Nil,
			Status:    http.StatusUnprocessableEntity,
			Message:   "SID is not a valid UUID",
			Err:       err,
			ListenURL: "SID: " + sid,
		})
		return
	}

	if !doTouchServerUpdate(c, &Server) {
		return
	}

	ypResponse(c, ypRes{
		SID:       Server.ID,
		Status:    http.StatusOK,
		Message:   "Successfully updated",
		Err:       nil,
		ListenURL: Server.ListenURL,
	})
}

func doRemoveGenresCodecsStream(stream *database.Stream) bool {
	for g, Genre := range stream.Genres {
		if Genre.StreamCount > 0 {
			Genre.StreamCount--
		}
		if database.Db.Model(stream.Genres[g]).Association("Streams").Count() == 1 {
			if err := database.Db.Delete(stream.Genres[g]).Error; err != nil {
				yplog.Err(err).Msg("Error while deleting genre")
			}
		} else {
			database.Db.Save(stream.Genres[g])
		}
	}

	for c, Codec := range stream.Codecs {
		if Codec.StreamCount > 0 {
			Codec.StreamCount--
		}
		database.Db.Save(stream.Codecs[c])
	}

	if len(stream.Servers) == 1 {
		database.Db.Model(stream).Association("Codecs").Clear()
		database.Db.Model(stream).Association("Genres").Clear()
		if err := database.Db.Delete(stream).Error; err != nil {
			yplog.Err(err).Msg("Error while deleting stream")
		}
	}
	return true
}

func doLoadServerForRemove(c *gin.Context, server *database.Server) (bool, string) {
	sid := c.PostForm("sid")
	if sid == "" {
		ypResponse(c, ypRes{
			SID:       uuid.Nil,
			Status:    http.StatusUnprocessableEntity,
			Message:   "No SID",
			Err:       nil,
			ListenURL: "",
		})
		return false, sid
	}

	var err error
	server.ID, err = uuid.FromString(sid)
	if err != nil {
		ypResponse(c, ypRes{
			SID:       uuid.Nil,
			Status:    http.StatusUnprocessableEntity,
			Message:   "SID is not a valid UUID",
			Err:       err,
			ListenURL: "SID: " + sid,
		})
		return false, sid
	}

	if database.Db.First(server).RecordNotFound() {
		ypResponse(c, ypRes{
			SID:       server.ID,
			Status:    http.StatusNotFound,
			Message:   "SID not found",
			Err:       nil,
			ListenURL: "",
		})
		return false, sid
	}
	return true, sid
}

func ypRemove(c *gin.Context) {
	var Server database.Server

	cont, sid := doLoadServerForRemove(c, &Server)
	if !cont {
		return
	}

	var Stream database.Stream
	if err := database.Db.Model(&Server).Preload("Servers").Preload("Genres").Preload("Codecs").Related(&Stream).Error; err != nil {
		ypResponse(c, ypRes{
			SID:       Server.ID,
			Status:    http.StatusInternalServerError,
			Message:   "Database error while removing stream",
			Err:       err,
			ListenURL: Server.ListenURL,
		})
		return
	}

	if !doRemoveGenresCodecsStream(&Stream) {
		return
	}

	if err := database.Db.Delete(&Server).Error; err != nil {
		ypResponse(c, ypRes{
			SID:       Server.ID,
			Status:    http.StatusInternalServerError,
			Message:   "Database error while removing server",
			Err:       err,
			ListenURL: Server.ListenURL,
		})
		return
	}

	ypResponse(c, ypRes{
		SID:       uuid.Nil,
		Status:    http.StatusOK,
		Message:   "Successfully removed",
		Err:       nil,
		ListenURL: "SID: " + sid,
	})
}

func postYp(c *gin.Context) {
	action := ypPostForm(c, "action")
	if action == "" {
		ypResponse(c, ypRes{
			SID:       uuid.Nil,
			Status:    http.StatusBadRequest,
			Message:   "Action not set",
			Err:       nil,
			ListenURL: "",
		})
		return
	}

	switch action {
	case "add":
		ypAdd(c)

	case "touch":
		ypTouch(c)

	case "remove":
		ypRemove(c)

	default:
		ypResponse(c, ypRes{
			SID:       uuid.Nil,
			Status:    http.StatusBadRequest,
			Message:   "Unsupported action",
			Err:       nil,
			ListenURL: "",
		})
		return
	}
}

// YPTouchCron is the cronjob that removes unresponsive servers
func YPTouchCron() {
	yplog.Debug().Msg("Removing unresponsive servers...")
	var Servers []database.Server
	database.Db.Where("last_touch < ?", time.Now().Add(-time.Second*time.Duration(config.C.YP.MissedTouchesThreshold*config.C.YP.TouchFreq))).Find(&Servers)
	for s := range Servers {
		var Stream database.Stream
		database.Db.Model(&Servers[s]).Preload("Servers").Preload("Genres").Preload("Codecs").Related(&Stream)

		for g, Genre := range Stream.Genres {
			if Genre.StreamCount > 0 {
				Genre.StreamCount--
			}
			if database.Db.Model(&Stream.Genres[g]).Association("Streams").Count() == 1 {
				database.Db.Delete(&Stream.Genres[g])
			} else {
				database.Db.Save(&Stream.Genres[g])
			}
		}

		for c, Codec := range Stream.Codecs {
			if Codec.StreamCount > 0 {
				Codec.StreamCount--
			}
			database.Db.Save(&Stream.Codecs[c])
		}

		if len(Stream.Servers) == 1 {
			database.Db.Model(&Stream).Association("Codecs").Clear()
			database.Db.Model(&Stream).Association("Genres").Clear()
			database.Db.Delete(&Stream)
		}

		database.Db.Delete(&Servers[s])
	}
}
