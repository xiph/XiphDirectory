// Package web provides the XiphDirectory web server implementation with the respective routes
package web

import (
	"html/template"
	"net/http"
	"net/url"
	"path"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"gitlab.xiph.org/xiph/XiphDirectory/internal/config"
	"gitlab.xiph.org/xiph/XiphDirectory/internal/ginzerolog"
)

var slog zerolog.Logger

// Init intizializes the web server
func Init() {
	slog = log.With().Str("component", "web").Logger()
	yplog = log.With().Str("component", "yp").Logger()
	r := gin.New()

	r.Use(gin.Recovery())
	r.Use(ginzerolog.SetLogger(ginzerolog.Config{
		Logger: &slog,
	}))

	r.SetFuncMap(template.FuncMap{"forceurlencode": url.PathEscape})

	r.ForwardedByClientIP = config.C.Web.TrustProxyHeaders
	r.AppEngine = false
	r.Use(cors.Default())

	r.UseRawPath = true

	r.LoadHTMLGlob(path.Join(config.C.Web.WebDir, "templates/*.tmpl"))
	r.Static("/static", path.Join(config.C.Web.WebDir, "static"))

	// YP-API v1
	r.POST("/cgi-bin/yp-cgi", postYp)
	r.GET("/cgi-bin/yp-cgi", func(c *gin.Context) {
		c.Header("Allow", "POST")
		c.Data(http.StatusMethodNotAllowed, "text/plain", []byte("Invalid method, POST must be used"))
	})

	// Frontend
	r.GET("/", getIndex)
	r.GET("/yp.xml", getYpXML)
	r.GET("/genres", getGenres)
	r.GET("/genres/:name", getGenre)
	r.GET("/codecs", getCodecs)
	r.GET("/codecs/:name", getCodec)
	r.GET("/search", getSearch)

	r.GET("/favicon.ico", func(c *gin.Context) {
		c.File(path.Join(config.C.Web.WebDir, "static/favicons/favicon.ico"))
	})

	if config.C.Web.BindUnix {
		go func() {
			err := r.RunUnix(config.C.Web.Bind)
			if err != nil {
				log.Fatal().Err(err).Msg("Can not start webserver")
			}
		}()
	} else {
		go func() {
			err := r.Run(config.C.Web.Bind)
			if err != nil {
				log.Fatal().Err(err).Msg("Can not start webserver")
			}
		}()
	}
	slog.Info().Msg("Server listening")
}
