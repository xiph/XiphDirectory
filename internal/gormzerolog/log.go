// Package gormzerolog provides a GORM-compatible logger interface to the zerolog logger
package gormzerolog

import (
	"fmt"
	"time"

	"github.com/jinzhu/gorm"
	"github.com/rs/zerolog"
)

type log struct {
	occurredAt time.Time
	source     string
	duration   time.Duration
	sql        string
	values     []string
	other      []string
	level      zerolog.Level
}

func (l *log) toZerologEvent(zll *zerolog.Logger) *zerolog.Event {
	if l.level == zerolog.ErrorLevel {
		return zll.Error().
			Time("occurredAt", l.occurredAt).
			Str("source", l.source).
			Dur("duration", l.duration).
			Str("sql", l.sql).
			Strs("values", l.values).
			Strs("other", l.other)
	}
	return zll.Trace().
		Time("occurredAt", l.occurredAt).
		Str("source", l.source).
		Dur("duration", l.duration).
		Str("sql", l.sql).
		Strs("values", l.values).
		Strs("other", l.other)
}

func createLog(values []interface{}) *log {
	ret := &log{}
	ret.occurredAt = gorm.NowFunc()

	if len(values) > 1 {
		var level = values[0]
		ret.source = getSource(values)

		if level == "sql" {
			ret.level = zerolog.TraceLevel
			ret.duration = getDuration(values)
			ret.values = getFormattedValues(values)
			ret.sql = values[3].(string)
		} else {
			ret.level = zerolog.ErrorLevel
			ret.other = append(ret.other, fmt.Sprint(values[2:]))
		}
	}

	return ret
}
