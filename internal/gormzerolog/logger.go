package gormzerolog

import (
	"github.com/rs/zerolog"
)

// New create logger object for *gorm.DB from *zap.Logger
func New(logger *zerolog.Logger) *Logger {
	return &Logger{
		zll: logger,
	}
}

// Logger is an alternative implementation of *gorm.Logger
type Logger struct {
	zll *zerolog.Logger
}

// Print passes arguments to Println
func (l *Logger) Print(values ...interface{}) {
	l.Println(values)
}

// Println format & print log
func (l *Logger) Println(values []interface{}) {
	createLog(values).toZerologEvent(l.zll).Send()
}
