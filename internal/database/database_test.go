package database

import (
	"database/sql"
	"testing"

	"github.com/DATA-DOG/go-sqlmock"
	"github.com/stretchr/testify/assert"
)

var slice = []int{1, 2, 3, 4, 5}
var sliceReversed = []int{5, 4, 3, 2, 1}

var sqldb *sql.DB
var mock sqlmock.Sqlmock

func init() {
	var err error
	sqldb, mock, err = sqlmock.New()
	if err != nil {
		panic(err)
	}
}

func TestReverseSlice(t *testing.T) {
	ReverseSlice(slice)
	assert.Equal(t, sliceReversed, slice, "The slice should be reversed")
}

func TestOpenUnknownDialect(t *testing.T) {
	err := Open("unknown", "host=localhost user=directory dbname=directory password=directory sslmode=disable")
	assert.EqualError(t, err, "sql: unknown driver \"unknown\" (forgotten import?)", "There should be an error on unknown dialect")
}

func TestOpenAndClose(t *testing.T) {
	var err error

	mock.ExpectExec("CREATE EXTENSION IF NOT EXISTS \"uuid-ossp\";")

	err = Open("postgres", sqldb)
	assert.NoError(t, err, "There should be no error")

	err = mock.ExpectationsWereMet()
	assert.NoError(t, err, "Expectations should have been met by now")

	assert.NotNil(t, Db, "Database should be opened before closing test")
	Close()
	assert.Nil(t, Db, "Database should be nil after closing")
}

func TestGenerateCursorNoStreams(t *testing.T) {
	cursors := GenerateCursor([]Stream{}, Db, false, "created_at", "ASC")
	assert.Equal(t, Cursors{}, cursors, "Cursors should be empty")
}
