// Package database provides the XiphDirectory database implementation and data models for it
package database

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"reflect"
	"time"

	"github.com/jinzhu/gorm"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	uuid "github.com/satori/go.uuid"
	"gitlab.xiph.org/xiph/XiphDirectory/internal/config"
	"gitlab.xiph.org/xiph/XiphDirectory/internal/gormzerolog"

	// Postgres dialect for gorm
	_ "github.com/lib/pq"
)

// Stream type
type Stream struct {
	ID            uuid.UUID `gorm:"primary_key; unique_index:idx_createdat_id;" sql:"type:uuid NOT NULL DEFAULT uuid_generate_v4()" xml:"-"`
	CreatedAt     time.Time `gorm:"unique_index:idx_createdat_id;" xml:"-"`
	UpdatedAt     time.Time `xml:"-"`
	StreamName    string    `gorm:"not null;" xml:"server_name"`
	StreamType    string    `gorm:"not null;" xml:"server_type"`
	Description   string    `xml:"-"`
	URL           string    `xml:"-"`
	Codecs        []Codec   `gorm:"many2many:stream_codecs;" json:",omitempty" xml:"-"`
	Bitrate       int       `xml:"bitrate"`
	Samplerate    int       `xml:"samplerate"`
	Quality       float32   `xml:"-"`
	Channels      int       `xml:"channels"`
	ClusterPass   string    `gorm:"index" json:"-" xml:"-"`
	Genres        []Genre   `gorm:"many2many:stream_genres;" json:",omitempty" xml:"-"`
	Servers       []Server  `json:",omitempty" xml:"-"`
	ListenURL     string    `gorm:"-" xml:"listen_url"`
	ListenURLLoad float32   `gorm:"-" xml:"-"`
	Listeners     uint      `xml:"-"`
	NowPlaying    string    `gorm:"-" xml:"current_song"`
	Genre         string    `gorm:"-" xml:"genre" json:"-"`
}

// Server type
type Server struct {
	ID           uuid.UUID `gorm:"primary_key;" sql:"type:uuid NOT NULL DEFAULT uuid_generate_v4()"`
	StreamID     uuid.UUID `sql:"type:uuid"`
	Stream       *Stream   `json:",omitempty"`
	ListenURL    string    `gorm:"default:NULL;unique_index:idx_listenurl;"`
	LastTouch    time.Time `gorm:"not null;"`
	NowPlaying   string
	Listeners    uint
	MaxListeners int
	IP           string `grom:"not null;" json:"-"`
}

// Codec type
type Codec struct {
	ID          uuid.UUID `gorm:"primary_key; unique_index:idx_codecs_name_id;" sql:"type:uuid NOT NULL DEFAULT uuid_generate_v4()"`
	Name        string    `gorm:"unique_index:idx_codecs_name_id"`
	StreamCount uint

	Streams []Stream `gorm:"many2many:stream_codecs;" json:",omitempty"`
}

// Genre type
type Genre struct {
	ID          uuid.UUID `gorm:"primary_key; unique_index:idx_genres_name_id" sql:"type:uuid NOT NULL DEFAULT uuid_generate_v4()"`
	Name        string    `gorm:"unique_index:idx_genres_name_id"`
	StreamCount uint

	Streams []Stream `gorm:"many2many:stream_genres;" json:",omitempty"`
}

type cursor struct {
	LastSort interface{} `json:"s,omitempty"`
	ID       uuid.UUID   `json:"i,omitempty"`
	Prev     bool        `json:"p,omitempty"`
}

// Cursors is a struct containing the cursors
type Cursors struct {
	Prev string
	Next string
}

// Db is the GORM database handle
var Db *gorm.DB

var slog zerolog.Logger

// ReverseSlice reverses a given slice
func ReverseSlice(s interface{}) {
	size := reflect.ValueOf(s).Len()
	swap := reflect.Swapper(s)
	for i, j := 0, size-1; i < j; i, j = i+1, j-1 {
		swap(i, j)
	}
}

// Paginate adds pagination parameters to gorm query and returns if result will be flipped
func Paginate(db *gorm.DB, cur string, order string, orderdir string) (*gorm.DB, bool) {
	var orderstmt, orderstmtflipped, wherestmt, wherestmtflipped string
	if orderdir == "ASC" {
		orderstmt = fmt.Sprintf("(%s, id) ASC", gorm.ToColumnName(order))
		orderstmtflipped = fmt.Sprintf("(%s, id) DESC", gorm.ToColumnName(order))
		wherestmt = fmt.Sprintf("(%s, id) > (?,?)", gorm.ToColumnName(order))
		wherestmtflipped = fmt.Sprintf("(%s, id) < (?,?)", gorm.ToColumnName(order))
	} else {
		orderstmt = fmt.Sprintf("(%s, id) DESC", gorm.ToColumnName(order))
		orderstmtflipped = fmt.Sprintf("(%s, id) ASC", gorm.ToColumnName(order))
		wherestmt = fmt.Sprintf("(%s, id) < (?,?)", gorm.ToColumnName(order))
		wherestmtflipped = fmt.Sprintf("(%s, id) > (?,?)", gorm.ToColumnName(order))
	}
	if cur == "" {
		return db.Order(orderstmt).Limit(config.C.Web.PageSize), false
	}

	bytes, err := base64.RawStdEncoding.DecodeString(cur)
	if err != nil {
		return db.Order(orderstmt).Limit(config.C.Web.PageSize), false
	}

	var curs cursor
	err = json.Unmarshal(bytes, &curs)
	if err != nil {
		return db.Order(orderstmt).Limit(config.C.Web.PageSize), false
	}

	if curs.Prev {
		return db.Where(wherestmtflipped, curs.LastSort, curs.ID).Order(orderstmtflipped).Limit(config.C.Web.PageSize), true
	}
	return db.Where(wherestmt, curs.LastSort, curs.ID).Order(orderstmt).Limit(config.C.Web.PageSize), false
}

// GenerateCursor generates as cursor for the given time and id
func GenerateCursor(obj []Stream, statement *gorm.DB, relation bool, order string, orderdir string) Cursors {
	var wherestmt, wherestmtflipped string
	if orderdir == "ASC" {
		wherestmt = fmt.Sprintf("(%s, id) > (?,?)", gorm.ToColumnName(order))
		wherestmtflipped = fmt.Sprintf("(%s, id) < (?,?)", gorm.ToColumnName(order))
	} else {
		wherestmt = fmt.Sprintf("(%s, id) < (?,?)", gorm.ToColumnName(order))
		wherestmtflipped = fmt.Sprintf("(%s, id) > (?,?)", gorm.ToColumnName(order))
	}
	var curs Cursors
	l := len(obj)
	if l == 0 {
		return curs
	}
	prevcnt := 0
	nextcnt := 0

	objPrev := reflect.ValueOf(obj[0]).FieldByName(order).Interface()
	objNext := reflect.ValueOf(obj[l-1]).FieldByName(order).Interface()

	if relation {
		var streams []Stream
		statement.Where(wherestmtflipped, objPrev, obj[0].ID).Related(&streams, "Streams")
		prevcnt = len(streams)
		statement.Where(wherestmt, objNext, obj[l-1].ID).Related(&streams, "Streams")
		nextcnt = len(streams)
	} else {
		statement.Where(wherestmtflipped, objPrev, obj[0].ID).Count(&prevcnt)
		statement.Where(wherestmt, objNext, obj[l-1].ID).Count(&nextcnt)
	}

	if prevcnt > 0 {
		var cur cursor
		cur.LastSort = objPrev
		cur.ID = obj[0].ID
		cur.Prev = true

		bytes, err := json.Marshal(&cur)
		if err != nil {
			curs.Prev = ""
		}

		curs.Prev = base64.RawStdEncoding.EncodeToString(bytes)
	}

	if nextcnt > 0 {
		var cur cursor
		cur.LastSort = objNext
		cur.ID = obj[l-1].ID
		cur.Prev = false

		bytes, err := json.Marshal(&cur)
		if err != nil {
			curs.Next = ""
		}

		curs.Next = base64.RawStdEncoding.EncodeToString(bytes)
	}

	return curs
}

// Init initzialises the database
func Init() {
	slog = log.With().Str("component", "database").Logger()
	err := Open("postgres", config.C.Database.Connection)
	if err != nil {
		slog.Fatal().Err(err).Msg("Could not establish connection")
	}
	slog.Info().Msg("Connection established")
}

// Open opens the database connection with given dialect and connection string
func Open(dialect string, connection interface{}) (err error) {
	Db, err = gorm.Open(dialect, connection)
	if err != nil {
		return err
	}

	dblogger := gormzerolog.New(&slog)
	Db.SetLogger(dblogger)
	Db.LogMode(true)

	Db.Exec("CREATE EXTENSION IF NOT EXISTS \"uuid-ossp\";")
	Db.AutoMigrate(&Stream{}, &Server{}, &Codec{}, &Genre{})

	// Trigger for added servers
	Db.Exec(`DROP TRIGGER IF EXISTS cascade_listeners_on_insert ON servers;
	CREATE OR REPLACE FUNCTION insert_listeners() RETURNS trigger AS $body$
	BEGIN
		UPDATE streams SET listeners = listeners + NEW.listeners WHERE id = NEW.stream_id;
		RETURN NEW;
	END;
	$body$ LANGUAGE plpgsql;
	CREATE TRIGGER cascade_listeners_on_insert AFTER INSERT ON servers FOR EACH ROW EXECUTE PROCEDURE insert_listeners();`)

	// Trigger for updated servers
	Db.Exec(`DROP TRIGGER IF EXISTS cascade_listeners_on_update ON servers;
	CREATE OR REPLACE FUNCTION update_listeners() RETURNS trigger AS $body$
	BEGIN
		UPDATE streams SET listeners = listeners - OLD.listeners + NEW.listeners WHERE id = NEW.stream_id;
		RETURN NEW;
	END;
	$body$ LANGUAGE plpgsql;
	CREATE TRIGGER cascade_listeners_on_update AFTER UPDATE ON servers FOR EACH ROW EXECUTE PROCEDURE update_listeners();`)

	// Trigger for deleted servers
	Db.Exec(`DROP TRIGGER IF EXISTS cascade_listeners_on_delete ON servers;
	CREATE OR REPLACE FUNCTION delete_listeners() RETURNS trigger AS $body$
	BEGIN
		UPDATE streams SET listeners = listeners - OLD.listeners WHERE id = OLD.stream_id;
		RETURN NEW;
	END;
	$body$ LANGUAGE plpgsql;
	CREATE TRIGGER cascade_listeners_on_delete AFTER DELETE ON servers FOR EACH ROW EXECUTE PROCEDURE delete_listeners();`)

	return nil
}

// Close closes the GORM database handle
func Close() {
	err := Db.Close()
	if err != nil {
		slog.Error().Err(err).Msg("Error while closing the database")
	}
	Db = nil
}
