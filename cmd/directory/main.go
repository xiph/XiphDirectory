package main

import (
	"flag"
	"fmt"
	"io"
	"os"
	"os/signal"
	"syscall"

	"github.com/robfig/cron"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"

	"gitlab.xiph.org/xiph/XiphDirectory/internal/bans"
	"gitlab.xiph.org/xiph/XiphDirectory/internal/config"
	"gitlab.xiph.org/xiph/XiphDirectory/internal/database"
	"gitlab.xiph.org/xiph/XiphDirectory/internal/rotate"
	"gitlab.xiph.org/xiph/XiphDirectory/internal/web"
)

var exitSig = make(chan int)
var logWriter *rotate.Writer

func main() {
	cfg := flag.String("c", "", "Config file")
	loglevel := flag.Int("loglevel", int(zerolog.InfoLevel), "Specifies the log level")
	prettylog := flag.Bool("prettylog", false, "Set to true for pretty log printing")
	flag.Parse()

	if *cfg == "" {
		log.Fatal().Msg("Config file must not be empty!")
	}

	err := config.LoadConfig(*cfg)
	if err != nil {
		log.Fatal().Err(err).Msg("Error loading config")
	}

	if config.C.Log.LogToFile {
		logWriter, _ = rotate.New(config.C.Log.Filename)
		if *prettylog {
			log.Logger = log.Output(io.MultiWriter(zerolog.ConsoleWriter{Out: os.Stderr, TimeFormat: "15:04:05"}, logWriter))
		} else {
			log.Logger = log.Output(logWriter)
		}
	} else if *prettylog {
		log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr, TimeFormat: "15:04:05"})
	}
	zerolog.SetGlobalLevel(zerolog.Level(*loglevel))
	log.Info().Msg("Server starting")

	err = bans.LoadBanlist(config.C.YP.Banlist)
	if err != nil {
		log.Error().Err(err).Msg("Error loading banlist")
	}
	database.Init()
	web.Init()

	cj := cron.New()
	cjlt := fmt.Sprintf("@every %ds", config.C.YP.TouchFreq/2)
	err = cj.AddFunc(cjlt, web.YPTouchCron)
	if err != nil {
		log.Warn().Err(err).Msg("Error adding the cleaner cron job")
	}
	go cj.Run()

	go signalHandler()
	exit := <-exitSig

	cj.Stop()
	database.Close()
	log.Info().Msg("Server stopped")
	os.Exit(exit)
}

func signalHandler() {
	c := make(chan os.Signal, 5)
	signal.Notify(c)

	for {
		sig := <-c
		switch sig {
		case syscall.SIGINT:
			exitSig <- 0
		case syscall.SIGHUP:
			bans.ReloadBanlist(config.C.YP.Banlist)
			if config.C.Log.LogToFile {
				err := logWriter.Rotate()
				if err != nil {
					log.Warn().Err(err).Msg("Could not rotate log!")
				}
			}
		case syscall.SIGURG:
			// Do nothing
		default:
			log.Warn().Interface("signal", sig).Msg("Received unknown signal, doing nothing")
		}
	}
}
