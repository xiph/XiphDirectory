# Version 1.0.1
## Fixes
* Serve favicon under `/favicon.ico` for backwards compatibility.
* Add `Allow: POST` header to the HTTP 405 response on GET requests to `/cgi-bin/yp-cgi`
* Segfault on write to non-createable log file. Logging to stderr in this case.

# Version 1.0.0
Initial release